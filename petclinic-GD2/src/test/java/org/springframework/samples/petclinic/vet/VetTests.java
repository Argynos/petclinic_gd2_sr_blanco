/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.vet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;

public class VetTests {
	
	public static Vet vet;
	public static Visit visit;
	public static Vet vet1;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		
		if (vet1 == null) {
			vet1 = new Vet();
			vet1.setFirstName("Roberto");
			vet1.setLastName("Rodriguez");
		}
		
	}
	
	@Before
	public void init() {
		vet1.setHomeVisits(false);
		
		if (visit == null) {
			visit = new Visit();
			visit.setId(1);
			visit.setDate(LocalDate.now());
			visit.setDescription("Descripcion");
		}
		
		if (vet == null) {
			vet = new Vet();
			vet.setFirstName("Roberto");
			vet.setLastName("Rodriguez");
			vet.addVisit(visit);
		}
		
	}
	
	@Test
	public void testVisits() {
		Visit visit2 = vet.getVisits().get(0);
		assertNotNull(vet.getVisits());
		assertNotNull(visit2);
		assertEquals(visit.getDescription(), visit2.getDescription());
		assertEquals(visit.getId(), visit2.getId());
		assertEquals(visit.getDate(), visit2.getDate());
	}
	
	@Test
	public void testVet1() {
		assertNotNull(vet1);				
	}
	
	@Test
	public void testFirstName() {
		assertNotNull(vet1.getFirstName());
		assertNotEquals(vet1.getFirstName(), "RoberTO");
		assertEquals(vet1.getFirstName(), "Roberto");		
	}
	
	@Test
	public void testLastName() {
		assertNotNull(vet1.getLastName());
		assertEquals(vet1.getLastName(), "Rodriguez");
		assertNotEquals(vet1.getLastName(), "Méndez");		
	}
	
	@Test
	public void testHomeVisits() {
		assertNotNull(vet1.getHomeVisits());
		assertFalse(vet1.getHomeVisits());
	}
	
	@Test
    public void testSerialization() {
        Vet vet = new Vet();
        vet.setFirstName("Zaphod");
        vet.setLastName("Beeblebrox");
        vet.setId(123);
        Vet other = (Vet) SerializationUtils
                .deserialize(SerializationUtils.serialize(vet));
        assertThat(other.getFirstName()).isEqualTo(vet.getFirstName());
        assertThat(other.getLastName()).isEqualTo(vet.getLastName());
        assertThat(other.getId()).isEqualTo(vet.getId());
    }
	
	@After
	public void finish() {
		this.visit = null;
		vet1.setHomeVisits(true);
	}

}