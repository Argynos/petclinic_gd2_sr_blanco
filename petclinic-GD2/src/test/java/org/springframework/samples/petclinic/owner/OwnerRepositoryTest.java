package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class OwnerRepositoryTest {
	@Autowired
	private OwnerRepository owners;
	
	private Owner owner;
	
	@Before
	public void init() {
		
		if (this.owner==null) {
			owner = new Owner ();
			owner.setFirstName("Luis");
			owner.setLastName("Gonzalez");
			owner.setAddress("Calle falsa 123");
			owner.setCity("Caceres");
			owner.setTelephone("927775566");
			
			this.owners.save(owner);
		}
	}
	
	@Test
	public void testDeleteById() {
		OwnerRepository expectedOwners = owners;
		Owner o = new Owner ();
		o.setFirstName("Luis Borrable");
		o.setLastName("Gonzalez Borrable");
		o.setAddress("Calle falsa 123 Borrable");
		o.setCity("Caceres Borrable");
		o.setTelephone("927445577");
		
		assertNull(o.getId());
		this.owners.save(o);
		assertNotNull(o.getId());
		
		this.owners.deleteById(o.getId());
		
		assertEquals(expectedOwners, owners);
	}
	
	@After
	public void finish() {
		this.owner=null;
	}
}
