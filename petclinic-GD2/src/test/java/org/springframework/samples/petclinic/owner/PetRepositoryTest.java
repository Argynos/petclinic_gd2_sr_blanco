package org.springframework.samples.petclinic.owner;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Collection;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class PetRepositoryTest {
	@Autowired
	private PetRepository pets;
	private Pet pet;
	
	
	@Before
	public void init() {
		
		if (this.pet==null) {
			PetType pt = new PetType();
			pt.setId(1);
			pt.setName("Canino");
			
			Owner owner= new Owner();
			owner.setFirstName("Luis");
			owner.setLastName("Gonzalez");
			owner.setAddress("Calle falsa 123");
			owner.setCity("Caceres");
			owner.setTelephone("927775566");
			owner.setId(1);
			
			pet = new Pet();
			pet.setName("PetName");
			pet.setBirthDate(LocalDate.now());
			pet.setComments("Coments 1");
			pet.setWeight(23);
			pet.setOwner(owner);
			pet.setType(pt);
			
			this.pets.save(pet);
		}
	}
	
	@Test
	public void testFindByName() {
		PetType pt = new PetType();
		pt.setId(1);
		pt.setName("Canino");
		Owner o= new Owner();
		o = new Owner ();
		o.setFirstName("Pedro");
		o.setLastName("Fernandez");
		o.setAddress("Calle nueva 123");
		o.setCity("Badajoz");
		o.setTelephone("927775566");
		o.setId(1);
		
		Pet p = new Pet();
		p.setName("Alex");
		p.setBirthDate(LocalDate.now());
		p.setComments("Ladra mucho");
		p.setWeight(14);
		p.setOwner(o);
		p.setType(pt);
		
		o.addPet(p);
		
		assertNull(p.getId());
		this.pets.save(p);
		assertNotNull(p.getId());
			
		
		Collection<Pet> petsFindsByName = this.pets.findPetsByName(p.getName());
		
		assertTrue(petsFindsByName.contains(p));
		
	}
	
	@Test
	public void testDelete() throws Exception {		
		assertNotNull(pet);	
		pets.delete(pet);	
		assertNull(pets.findById(pet.getId()));
	}
	
	@Test
	public void testDeletePetInfo() throws Exception {			
		assertNotNull(pet.getId());
		pets.delete(pet);		
		pets.deletePetInfo(pet.getId());
		assertNull(pets.findById(pet.getId()));
	}
	
	@After
	public void finish() {
		this.pet=null;
	}
}
